<?php

namespace App\Services;

class Slot
{
    /** 
     * Total budget of the user
     * 
     * @var int
     */
    protected $budget;

    /** 
     * Bet amount, always 100 cents (1 Euro)
     * 
     * @var int
     */
    protected $betAmount = 100;

    /** 
     * Symbols to be shuffled
     * 
     * @var array
     */
    protected $symbols = ['9', '10', 'J', 'Q', 'K', 'A', 'Cat', 'Dog', 'Monkey', 'Bird'];

    /**
     * @var Spinner
     */
    protected $spinner;

    /**
     * @var Payer
     */
    protected $payer;

    public function __construct(
        Spinner $spinner,
        Payer $payer
    ) {
        $this->spinner = $spinner;
        $this->payer = $payer;
    }

    public function spin(): array
    {
        $lines = $this->spinner->run(collect($this->symbols));
        $wins = $this->payer
            ->forLines($lines)
            ->calculate($this->betAmount);
        
        // set left budget
        $this->budget += $wins['total_win'] - $this->betAmount;
        
        return array_merge([
            'board' => '[' . implode(', ', $lines) . ']',
            'bet_amount' => $this->betAmount,
        ], $wins);
    }

    public function setBudget(int $budget): void
    {
        $this->budget = $budget;
    }

    public function canSpin(): bool
    {
        return $this->betAmount <= $this->budget;
    }
}
