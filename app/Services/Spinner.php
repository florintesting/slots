<?php

namespace App\Services;

use Illuminate\Support\Collection;

class Spinner
{
    /** 
     * Mapping each slot element
     */
    const MAP = [
        0 => null,
        3 => null,
        6 => null,
        9 => null,
        12 => null,
        1 => null,
        4 => null,
        7 => null,
        10 => null,
        13 => null,
        2 => null,
        5 => null,
        8 => null,
        11 => null,
        14 => null,
    ];

    public function run(Collection $symbols): array
    {
        return $this->shuffle($symbols)
            ->toArray();
    }

    protected function shuffle(Collection $symbols): Collection
    {
        return collect(self::MAP)
            ->map(function ($elem) use ($symbols): string {
                return $symbols
                    ->random(1)
                    ->implode(',');
            });
    }
}
