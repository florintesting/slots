<?php

namespace App\Services;

use Illuminate\Support\Collection;

class Payer
{
    const PAYLINES = [
        [0, 3, 6, 9, 12],
        [1, 4, 7, 10, 13],
        [2, 5, 8, 11, 14],
        [0, 4, 8, 10, 12],
        [2, 4, 6, 10, 14],
    ];

    const PAYMENTS = [
        0 => 0,
        3 => 0.2,
        4 => 2,
        5 => 10,
    ];

    /** @var array */
    protected $lines;

    public function forLines(array $lines): Payer
    {
        $this->lines = $lines;

        return $this;
    }

    public function calculate(int $betAmount): array
    {
        $payments = collect(self::PAYLINES)
            ->map(function ($payline) use ($betAmount): array {
                $same = $this->same($payline);

                return [
                    'payline' => '[' . implode(', ', $payline) . ']: ' . $same,
                    'win' => (int) (self::PAYMENTS[$same] * $betAmount),
                ];
            })->filter(function ($payment): bool {
                return $payment['win'] > 0;
            });
        
        return [
            'total_win' => $payments->pluck('win')->sum(),
            'paylines' => $payments->pluck('payline')->implode(','),
        ];
    }


    /**
     * Returns the total number of same elements from a payline
     */
    protected function same(array $payline): int
    {
        $same = 0;
        
        if ($this->lines[$payline[0]] === $this->lines[$payline[1]] && $this->lines[$payline[1]] === $this->lines[$payline[2]]) {
            $same = 3;

            for ($i = 2; $i < 4; $i++) {
                if ($this->lines[$payline[$i]] === $this->lines[$payline[$i + 1]]) {
                    $same++;
                }
            }
        }

        return $same;
    }
}
