<?php

namespace App\Console\Commands;

use App\Services\Slot;
use Illuminate\Console\Command;

class SlotSpin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slots:spin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Spin the wheels';

    /**
     * @var Slot
     */
    protected $slot;

    public function __construct(Slot $slot)
    {
        parent::__construct();

        $this->slot = $slot;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $turn = 1;

        $this->slot->setBudget(500);

        while ($this->slot->canSpin()) {
            $this->info('Spine no ' . $turn . ':');
            $results = $this->slot->spin();
            print_r($results);

            sleep(1);
            $turn++;
        }

        $this->error('Stop!');
        $this->line('User ran out of budget.');
    }
}
